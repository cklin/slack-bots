/**
 * Created by cklin on 2017/6/29.
 */

var n = require('child_process').fork('./child.js');

n.on('message', function(m) {
    console.log('PARENT got message:', m);
});

n.send({ hello: 'world' });
var XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;

if (!process.env.token) {
    console.log('Error: Specify token in environment');
    process.exit(1);
}

var Botkit = require('./lib/Botkit.js');
var os = require('os');

var controller = Botkit.slackbot({
    debug: false
});
var Locker = require('lock-queue');
var locker = new Locker();

var bot = controller.spawn({
    token: process.env.token
}).startRTM();
var time = 0;

controller.on(
    'direct_message,direct_mention,mention,ambient', function(bot, message) {

        // var exec = require('child_process').exec;

        console.log(message.user);
        changeIdToName(bot, message.user, function(response){
            console.log(time);
            setTimeout(function(){
                var exec = require('child_process').exec, child;
                var format_message = message.text.replace(/<@([A-Za-z0-9])+>/, "");
                require('child_process').exec('say "'+response.user.real_name+' 說 '+format_message+'"  -v Mei-Jia',
                    function (error, stdout, stderr) {
                        if (error !== null) {
                            console.log('exec error: ' + error);
                        }
                    });
            }, 5000 * time);
            time = time + 1;
            setTimeout(function(){
                time = time - 1;
            }, 1000);
        });
    });
function getRandomId(bot, id, callback) {

    bot.api.groups.info({token: process.env.token ,channel: id},function(err,response) {
        return callback(response);
    });


}
function getChannelsList(bot, callback) {

    bot.api.channels.list({token: process.env.token },function(err,response) {
        return callback(response);
    });


}
function changeIdToName(bot, id, callback) {

    bot.api.users.info({token: process.env.token,user: id},function(err,response) {
        return callback(response);
    });


}